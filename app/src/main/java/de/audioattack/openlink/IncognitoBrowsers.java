package de.audioattack.openlink;

/**
 * Created by low012 on 23.10.17.
 */

public final class IncognitoBrowsers {

    public static final IncognitoBrowser FIREFOX =
            new IncognitoBrowser(
                    "org.mozilla.firefox",
                    "org.mozilla.gecko.LauncherActivity",
                    2015503969,
                    "private_tab");

    public static final IncognitoBrowser FENNEC =
            new IncognitoBrowser(
                    "org.mozilla.fennec_fdroid",
                    "org.mozilla.gecko.LauncherActivity",
                    550000,
                    "private_tab");

    // does not exist yet, but maybe one day...
    public static final IncognitoBrowser ICECAT =
            new IncognitoBrowser(
                    "public static final ",
                    "org.mozilla.gecko.LauncherActivity",
                    550000,
                    "private_tab");

    public static final IncognitoBrowser JELLY =
            new IncognitoBrowser(
                    "org.lineageos.jelly",
                    "org.lineageos.jelly.MainActivity",
                    1,
                    "extra_incognito");

    private IncognitoBrowsers() {
    }
}


